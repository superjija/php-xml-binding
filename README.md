# PHP XML binding

This project is based on Christoph Gockel's project XML-Data-Binding for PHP (https://github.com/christophgockel/pibx). It was rewritten for PHP 7.0. It supports XSD restrictions, class namespaces, type checks in function parameters...

Generate PHP classes based on a XML-Schema. These classes can be used to marshal the informations to XML or unmarshal from XML to PHP objects.

## Installation

via composer: 
```sh
composer require jija/php-xml-binding
```
or you can just download project source files directly

## Usage

with composer: 
```php
<?php
require __DIR__.'/vendor/autoload.php';
```

without composer 
```php
<?php
require 'path-to-php-xml-binding-dir/autoload.php';
```
